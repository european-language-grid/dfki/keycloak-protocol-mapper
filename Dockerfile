FROM openjdk:11-jdk

ENV TARGETDIR /elg/
RUN mkdir -p $TARGETDIR
ADD target/keycloak-customProtocolMapper-1.0-SNAPSHOT.jar ${TARGETDIR}/mapper.jar
WORKDIR ${TARGETDIR}